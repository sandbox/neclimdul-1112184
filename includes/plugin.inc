<?php
/**
 * @file
 * Drupal core plugin functionality.
 *
 * TODO Plugns should probably provide thier own lighter weight registry. We
 * currently don't have a way to track down the type and id during autoload
 * so this isn't really possible yet. Namespaces?
 * TODO - Please document me!
 */

// TODO Remove this once core supports file_scan_directory earlier.
// Core should allow access to file_scan_directory right off the bat. Its a
// core framework function with no database interaction and we need it to
// locate plugin definition files early in the boot process.
require_once DRUPAL_ROOT . '/includes/file.inc';

/**
 * Implements hook_plugin_types().
 *
 * This is a special case of the hook because all other versions must be
 * defined by modules. This version is used exclusively by the
 * PluginProviderCore provider class to load plugin types prior to the module
 * system being loaded.
 */
function drupal_plugin_types() {
  return array(
    'cache' => array(
      'cache' => FALSE,
      'discovery method' => 'PluginDiscoveryInclude',
    ),
  );
}

/**
 * @class Exception class used as a base for plugin related errors.
 */
class DrupalPluginException extends Exception { }

/**
 * Exception thrown when a plugin type requires an invalid discovery object.
 */
class DrupalPluginDiscoveryException extends DrupalPluginException { }

/**
 * Exception thrown when a plugin type requires an invalid registry object.
 */
class DrupalPluginRegistryException extends DrupalPluginException { }

/**
 * Exception thrown when a plugin type requires an invalid provider object.
 */
class DrupalPluginProviderException extends DrupalPluginException { }

/**
 * Factory like class for getting registry instances and type definitions.
 */
abstract class PluginRegistry {

  // Local type info cache. Wrapped around drupal_static.
  static protected $type_info;

  /**
   * Get a registry object for requesting plugin definitions.
   *
   * @param $owner
   *  The name of the module that implements this plugin type.
   * @param $type
   *  The type identifier of the plugin.
   *
   * @return PluginRegistryInterface
   *  A plugin registry object for requesting plugin definitions.
   *
   * @throws DrupalPluginDiscoveryException
   *  If the specified discovery object doesn't exist.
   * @throws DrupalPluginRegistryException
   *  If the specified registry object doesn't exist.
   * @throws DrupalPluginProviderException
   *  If one of the specified provider objects doesn't exist.
   */
  final public static function getInstance($owner, $type) {
    // Fetch the plugin type definition.
    $info = self::getTypeDefinition($owner, $type);

    if (empty($info)) {
      throw new DrupalPluginException('Missing type definition for ' . $type);
    }

    if (!class_exists($info['discovery method'])) {
      // With a invalid discovery class we're in an invalid state so we have to throw an exception.
      throw new DrupalPluginDiscoveryException('Invalid plugin discovery method ' . $info['discovery method']);
    }

    if (!class_exists($info['registry'])) {
      // With a invalid registry we're in an invalid state so we have to throw an exception.
      throw new DrupalPluginRegistryException('Invalid plugin registry object ' . $info['registry']);
    }

    $providers = array();
    foreach ($info['providers'] as $provider) {
      if (!class_exists($provider)) {
        // With a invalid provider we're in an invalid state so we have to
        // throw an exception.
        // TODO - evaluate if this really is an invalid state or we can just
        // assume we continue with the valid providers.
        throw new DrupalPluginProviderException('Invalid plugin provider object ' . $provider);
      }

      $providers[] = new $provider($info);
    }

    $discovery_method = new $info['discovery method']($info, $providers);
    return new $info['registry']($info, $discovery_method);
  }

  /**
   * Ask a module for info about a particular plugin type.
   *
   * @param $owner
   *   The name of the module that implements this plugin type.
   * @param $type
   *   The type identifier of the plugin.
   * @return
   *   A plugin definition array.
   */
  final public static function getTypeDefinition($owner, $type) {

    // Make sure type cache is initialized.
    self::initCache();

    if (!isset(self::$type_info[$owner])) {
      self::parseTypeDefinition($owner);
    }

    return isset(self::$type_info[$owner][$type]) ?
      self::$type_info[$owner][$type] :
      FALSE;
  }

  /**
   * Return the full list of plugin type info for all plugin types registered in
   * the current system.
   *
   * This function manages its own cache getting/setting, and should always be
   * used as the way to initially populate the list of plugin types. Make sure you
   * call this function to properly populate the plugin_types static
   * variable.
   *
   * @return array
   *   A multilevel array of plugin type info, the outer array keyed on module
   *   name and each inner array keyed on plugin type name.
   */
  final public static function getTypeDefinitions() {

    // Make sure type cache is initialized.
    self::initCache();

    if (drupal_bootstrap() == DRUPAL_BOOTSTRAP_FULL) {
      $containers = module_implements('plugin_types');
    }
    $containers[] = 'drupal';

    foreach ($containers as $owner) {
      if (!isset(self::$type_info[$owner])) {
        self::parseTypeDefinition($owner);
      }
    }

    return self::$type_info;
  }

  /**
   * Parse plugin type definition and setup defaults.
   */
  final protected static function parseTypeDefinition($owner) {

      self::$type_info[$owner] = array();

      // We can't call module invoke here as would make sense because some
      // core providers need to be invoked before the module system is
      // available.
      $function = $owner . '_plugin_types';
      $infos = $function();

      foreach ($infos as $plugin_type_name => $plugin_type_info) {
        // Apply defaults. Array addition will not overwrite pre-existing keys.
        $plugin_type_info += array(
          // Method used for discovering plugins. Pluggable as a class
          // implementing PluginDiscoveryInterface
          'discovery method' => 'PluginDiscoveryHook',
          'registry' => 'PluginRegistryDefault',
          'providers' => array('PluginProviderCore'),

          // A list of classes to provide to Drupal's autoloader.
          'classes' => array(),

          // Store the module and plugin type name for future reference.
          'owner' => $owner,
          'owner type' => $plugin_type_name,

          // Extra default values each plugin definition will recieve.
          'defaults' => array(),

          // Extension used by include based discovery plugins.
          'extension' => 'inc',

          // Hook extension used by providers to build function callbacks.
          'hook' => $owner . '_' . $plugin_type_name,

          // Cache and other non-implemented stuffs from ctools.
          'cache' => FALSE,
          'cache table' => 'cache',
          'process' => '',
          'load themes' => FALSE,
        );
        self::$type_info[$owner][$plugin_type_name] = $plugin_type_info;
      }
  }

  /**
   * Initialize local cache of plugin type definitions.
   */
  final protected static function initCache() {
    if (!isset(self::$type_info)) {
      // This could be replaced by a public method but this works for Drupal.
      self::$type_info = &drupal_static(__CLASS__ . ':' . __FUNCTION__, array());
    }
  }
}

/**
 * Interface for plugin registry objects.
 *
 * Plugin registry objects are used for requesting information about plugins.
 */
interface PluginRegistryInterface {
  /**
   * Standard constructor.
   */
  public function __construct(array $info, PluginDiscoveryInterface $discovery_method);

  /**
   * Retrieve plugin definition for the named plugin of this type.
   */
  public function getPluginDefinition($name);

  /**
   * Retrieve plugin definition for all plugins of this type.
   */
  public function getPluginDefinitions();

  /**
   * Get plugin type definition.
   */
  public function getTypeDefinition();

  /**
   * Retrieve a list of titles human readable titles suitable for placing in an #options array.
   */
  public function getPluginTitles();
}

/**
 * Default plugin registry object.
 *
 * This registry does a basic mapping of plugin information requests.
 */
class PluginRegistryDefault implements PluginRegistryInterface {

  protected $info = array();
  protected $discoveryMethod;
  protected $pluginDefinitions = array();

  public function __construct(array $info, PluginDiscoveryInterface $discovery_method) {

    // Store our definition for reference.
    $this->info = $info;

    // Instantiate our discovery method.
    $this->discoveryMethod = $discovery_method;
  }

  public function getPluginDefinition($name) {
    if (!isset($this->pluginDefinitions[$name])) {
      $this->pluginDefinitions[$name] = $this->discoveryMethod->findSingle($name);
    }

    return $this->pluginDefinitions[$name];
  }

  public function getPluginDefinitions() {
    // Currently we don't know when we've done a full load so this can't be cached.
    // TODO Figure out when a full load has been done and return local cache.
    $this->pluginDefinitions = $this->discoveryMethod->find();
    return $this->pluginDefinitions;
  }

  public function getTypeDefinition() {
    return $this->info;
  }

  public function getPluginTitles() {
    // TODO This is a method required for admin interfaces.
  }
}

/**
 * Basic interface for discovering plugins.
 */
interface PluginDiscoveryInterface {

  /**
   * Creates a PluginDiscovery instance.
   *
   * @param $info
   *   A plugin type definition array.
   * @param $providers
   *   An array or plugin providers.
   */
  public function __construct(array $info, array $providers);

  /**
   * Load a full list of plugin definitions arrays.
   *
   * @return
   *   An array of plugin definitions supplied by any hook implementations.
   */
  public function find();

  /**
   * Load a single plugin definition.
   *
   * @param $id
   *   The id of the plugin definition we're looking for.
   *
   * @return
   *   An array of plugin definitions supplied by any implementations.
   */
  public function findSingle($id);
}

/**
 * Base class for general plugin discovery methods.
 */
abstract class PluginDiscoveryBase implements PluginDiscoveryInterface {

  // Local storage for our plugin type info array for later reference.
  protected $info;
  // Strategy pattern storage for our provider objects.
  protected $providers;

  public function  __construct(array $info, array $providers) {
    $this->info = $info;
    $this->providers = $providers;
  }

  /**
   * Fill in default values and run hooks for plugin definitions.
   */
  protected function setDefaults($plugin_definition, $plugin_owner, $plugin_owner_type, $path, $file = NULL) {
    // Fill in global defaults.
    $plugin_definition += array(
      // Store the plugin name and path to the file.
      'path' => $path,
      'file' => $file,
      // Store the plugin owner in the definition for convenience.
      'owner' => $plugin_owner,
      'owner type' => $plugin_owner_type,
    );

    // Fill in plugin-specific defaults, if they exist.
    // TODO - should this be delegated to more specific discovery objects?
    if (!empty($this->info['defaults'])) {
      if (is_array($this->info['defaults'])) {
        $plugin_definition += $this->info['defaults'];
      }
    }

    return $plugin_definition;
  }
}

/**
 * Find a list of plugin definitions for hooks.
 */
class PluginDiscoveryHook extends PluginDiscoveryBase {
  protected $plugins;

  public function find() {

    // Populate the plugin cache.
    if (!isset($this->plugins)) {
      $this->plugins = array();

      foreach ($this->providers as $provider) {
        $type = $provider->getType();
        foreach ($provider->getFunctions() as $container => $function) {

          $data = $function();
          if (!empty($data)) {
            // TODO just use drupal_get_path($type, $container)?
            $path = $provider->getPath($container);
            foreach ($data as $name => $plugin_definition) {
              $data[$name] = $this->setDefaults($plugin_definition, $container, $type, $path);
            }
            if (is_array($data)) {
              $this->plugins += $data;
            }
          }
        }
      }
    }

    return $this->plugins;
  }

  public function findSingle($id) {

    // Check if plugin cache is populated.
    if (!isset($this->plugins)) {
      $this->find();
    }

    return isset($this->plugins[$id]) ? $this->plugins[$id] : array();
  }
}

/**
 * Base class for discovering plugins based on includes.
 */
abstract class PluginDiscoveryIncludeBase extends PluginDiscoveryBase {

  /**
   * Helper method whereby include based plugins can handle the logic of parsing
   * plugin definitions from the include file.
   */
  abstract protected function parseInclude($file);

  public function find() {

    $plugins = array();

    foreach ($this->providers as $provider) {
      $type = $provider->getType();
      foreach ($provider->getFiles() as $container => $files) {
        foreach ($files as $file) {
          $plugin_definition = $this->parseInclude($file);

          // TODO Move this processing out a level of the array by baking in file stuff.
          // If we have plugin information, process it and setup any default values.
          if ($plugin_definition) {
            $plugins[$file->name] = $this->setDefaults($plugin_definition, $container, $type, dirname($file->uri), basename($file->uri));
          }
          else {
            $plugins[$file->name] = array();
          }
        }
      }
    }

    return $plugins;
  }

  public function findSingle($id) {

    $plugins = array();

    // Iterate through all the plugin .inc files, load them and process the hook
    // that should now be available.
    foreach ($this->providers as $provider) {
      $type = $provider->getType();
      foreach ($provider->getFiles() as $container => $files) {
        foreach ($files as $file) {
          if (empty($files[$id])) {
            continue;
          }
          else {
            $file = $files[$id];
            $plugin_definition = $this->parseInclude($file);

            // If we have plugin information, process it and return. We build an
            // array of plugins because while settings the defaults processing,
            // child plugin logic could create more entries.
            if ($plugin_definition) {
              $plugins[$file->name] = $this->setDefaults($plugin_definition, $container, $type, dirname($file->uri), basename($file->uri));
            }
            else {
              $plugins[$file->name] = array();
            }
          }
        }
      }
    }

    return isset($plugins[$id]) ? $plugins[$id] : array();
  }
}

/**
 * Load plugin definition from include files.
 */
class PluginDiscoveryInclude extends PluginDiscoveryIncludeBase {

  protected function parseInclude($file) {
    // We store a static of plugin arrays for reference later because the
    // includes can't be re-included.
    static $plugin_arrays = array();

    if (!isset($plugin_arrays[$file->uri])) {

      require_once DRUPAL_ROOT . '/' . $file->uri;
      // If the .inc file set the quasi-global $plugin array, we can use that and
      // not call any additional functions.
      if (isset($plugin)) {
        $plugin_arrays[$file->uri] = $plugin;
      }
      else {
        // Cache an empty plugin definition so we don't try to parse it again.
        $plugin_arrays[$file->uri] = array();
      }
    }

    return $plugin_arrays[$file->uri];
  }
}

/**
 * Load plugin definition from info files.
 */
class PluginDiscoveryInfo extends PluginDiscoveryIncludeBase {

  protected function parseInclude($file) {
    return drupal_parse_info_file($file->uri);
  }
}


/**
 * Interface for getting information about plugin providers.
 */
interface PluginProviderInterface {

  public function __construct($info);

  /**
   * Get a list of provided functions.
   */
  public function getFunctions();

  /**
   * Get a list of provided defintion files.
   */
  public function getFiles();

  /**
   * Get the type being wrapped by the provider.
   *
   * This will generally be module, theme or core.
   */
  public function getType();

  /**
   * Get the path to a specific plugin containers definitions.
   *
   * This should be a direct path to a folder containing any plugin definitions
   * for the container.
   */
  public function getPath($container);
}

/**
 * Implementation of a core plugin provider.
 *
 * This wraps a set of simple hard coded plugins for core.
 */
class PluginProviderCore implements PluginProviderInterface {
  protected $info;

  // Local variable storing locations of plugins.
  protected $locations = array();

  public function __construct($info) {
    $this->info = $info;

    // Location of default core plugins.
    $this->locations['drupal'] = 'includes/plugins/' . $info['owner type'];
    // Location of all site plugins.
    // TODO - This is a poor location container name...
    $this->locations['sites_all'] = 'sites/all/plugins/' . $info['owner type'];

    // TODO really we should support something like this. conf_path?
    // $this->locations[] = 'sites/example.com/plugins';

    // TODO maybe this too. Do we treat profiles as part of core? Probably
    // have to. ie. a profile cache plugin.
    // $this->locations[] = 'profiles/myprofile/plugins';
  }

  public function getFunctions() {
    $function = 'core_' . $this->info['hook'];
    // TODO Evaluate if functions can "exist" outside of core or if it matters.
    // The 'drupal' maps to getPath calls in PluginDisccoveryHook for possibly
    // mapping to includes if needed. If those includes where outside of core
    // drupal, the current setup would be broken.
    return function_exists($function) ? array('drupal' => $function) : array();
  }

  public function getFiles() {
    foreach ($this->locations as $container => $path) {
      $files[$container] = file_scan_directory($path, '/\.' . $this->info['extension'] . '$/', array('key' => 'name'));
    }
    return $files;
  }

  public function getType() {
    return 'core';
  }

  public function getPath($container) {
    return $this->locations[$container];
  }
}

/**
 * Implementation of a module plugin provider.
 *
 * Modules are well suited for being plugin providers and this mostly wraps
 * existing Drupal methods like module_implements.
 */
class PluginProviderModule implements PluginProviderInterface {
  protected $info;

  public function __construct($info) {
    $this->info = $info;
  }

  public function getFunctions() {
    $functions = array();
    foreach (module_implements($this->info['hook']) as $module) {
      $functions[$module] = $module . '_' . $this->info['hook'];
    }
    return $functions;
  }

  public function getFiles() {
    $files = array();
    foreach (module_implements('plugin_directory') as $module) {
      $path = $this->getPath($module);
      if ($path) {
        $files[$module] = file_scan_directory($path, '/\.' . $this->info['extension'] . '$/', array('key' => 'name'));
      }
    }
    return $files;
  }

  public function getType() {
    return 'module';
  }

  public function getPath($container) {
    $sub_path = module_invoke($container, 'plugin_directory', $this->info['owner'], $this->info['owner type']);
    if ($sub_path) {
      return drupal_get_path('module', $container) . '/' . $sub_path;
    }
    return FALSE;
  }
}

/**
 * Implementation of a theme plugin provider.
 *
 * Themes are kinda of a odd case so we do our best to wrap up themes and
 * make them behave sanely in a way that's similar to modules. Because of how
 * goofy theme inheritance is, this is going to be a sticky provider.
 *
 * @TODO Actually implement the methods.
 */
class PluginProviderTheme implements PluginProviderInterface {
  protected $info;

  public function __construct($info) {
    $this->info = $info;
  }

  public function getFunctions() {
    return array();
  }

  public function getFiles() {
    return array();
  }

  public function getType() {
    return 'theme';
  }

  public function getPath($container) {
    return drupal_get_path('theme', $container);
  }

  /**
   * Helper function to build a plugin-friendly list of themes capable of
   * providing plugins.
   *
   * @return array $themes
   *   A list of themes that can act as plugin providers, sorted parent-first with
   *   the active theme placed last.
   */
  protected function listThemes() {
    static $themes;
    if (is_null($themes)) {
      $current = variable_get('theme_default', FALSE);
      $themes = $active = array();
      $all_themes = list_themes();
      foreach ($all_themes as $name => $theme) {
        // Only search from active themes
        if (empty($theme->status) && $theme->name != $current) {
          continue;
        }
        $active[$name] = $theme;
      }

      // Construct a parent-first list of all themes
      foreach ($active as $name => $theme) {
        $base_themes = isset($theme->base_themes) ? $theme->base_themes : array();
        $themes = array_merge($themes, $base_themes, array($name => $theme->info['name']));
      }
      // Put the actual theme info objects into the array
      foreach (array_keys($themes) as $name) {
        if (isset($all_themes[$name])) {
          $themes[$name] = $all_themes[$name];
        }
      }

      // Make sure the current default theme always gets the last word
      if ($current_key = array_search($current, array_keys($themes))) {
        $themes += array_splice($themes, $current_key, 1);
      }
    }
    return $themes;
  }
}
